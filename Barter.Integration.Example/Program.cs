﻿using Barter.Integration.Helpers;
using Barter.Integration.Models;
using Barter.Integration.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
namespace Barter.Integration.Example
{
    class Program
    {
        static void Main(string[] args)
        {
            Driver driver = new Driver("APIKEY", "Environment");
            //FeeHelper feesHelper = new FeeHelper();
            //var cardCreationFeesResponse = feesHelper.GetCardCreationFees(driver).Result;
            //var deserializedCardCreationFeesResponse = JsonConvert.DeserializeObject<CardCreationFeeResponse>(cardCreationFeesResponse);
            //var cardFxRatesResponse = feesHelper.GetExchangeRates(driver).Result;
            //var deserializedCardFxFeesResponse = JsonConvert.DeserializeObject<FxRatesResponse>(cardFxRatesResponse);
            //CustomerHelper customerHelper = new CustomerHelper();
            //var createCustomer = new CreateCustomer { account_number = "2347068264085", bank_code = "", bvn = "", email = "info@wallet.ng", firstname = "John", lastname = "Oke", };
            //var createCustomerResponse = customerHelper.CreateCustomer(createCustomer, driver).Result;

            //var deserializeCreateCustomerResponse = JsonConvert.DeserializeObject<CreateCustomerResponse>(createCustomerResponse);
            //if (deserializeCreateCustomerResponse != null && deserializeCreateCustomerResponse.status == "success")
            //{
            //    Console.WriteLine(deserializeCreateCustomerResponse.data.user.id);
            //    var response = customerHelper.GetCustomerByParameter("email", deserializeCreateCustomerResponse.data.user.email, driver).Result;
            //    var deserializedResponse = JsonConvert.DeserializeObject<GetCustomerByIdResponse>(response);
            //    if (deserializedResponse != null && deserializedResponse.status == "success")
            //    {

            //    }
            //    else
            //    {
            //        var deserializeCreateCustomerErrorResponse = JsonConvert.DeserializeObject<ErrorResponse>(response);
            //        Console.WriteLine(deserializeCreateCustomerErrorResponse.message);
            //    }
            //}
            //else
            //{
            //    var deserializeCreateCustomerErrorResponse = JsonConvert.DeserializeObject<ErrorResponse>(createCustomerResponse);
            //    var createCustomerErrorResponseData = deserializeCreateCustomerResponse.data;
            //    var deserializedCreateCustomerErrorResponseData = JsonConvert.DeserializeObject<EmailError>(createCustomerErrorResponseData.ToString());
            //    Console.WriteLine(deserializedCreateCustomerErrorResponseData.email[0]);
            //}
            //var response = customerHelper.GetCustomerByParameter("email", "john@wallet.ng", driver).Result;
            //var deserializedResponse = JsonConvert.DeserializeObject<GetCustomerByParamResponse>(response);
            //Console.WriteLine(deserializedResponse.data.id);
            //var responseById = customerHelper.GetCustomer(deserializedResponse.data.id, driver).Result;
            //var deserializedResponseById = JsonConvert.DeserializeObject<GetCustomerByIdResponse>(responseById);
            //var cardsResponse = customerHelper.GetCustomerCards("ac0a96a0-1702-11e8-b82c-71a6649c9775", driver).Result;
            //var deserializedCardsResponse = JsonConvert.DeserializeObject<GetCustomerCardsResponse>(cardsResponse);
            CardHelper cardHelper = new CardHelper();
            //CreateCard createCard = new CreateCard
            //{
            //    amount = "10",
            //    currency = "USD",
            //    card_band = "mastercard",
            //    card_type = "general",
            //    customer_id = "ac0a96a0-1702-11e8-b82c-71a6649c9775",
            //    name = "John Oke"
            //};
            //var cardResponse = cardHelper.CreateCard(createCard, driver).Result;
            //var deserializedCardResponse = JsonConvert.DeserializeObject<CardResponse>(cardResponse);
            //var response = cardHelper.GetCardDetails("f8e84020-17c5-11e8-94d5-1d3b60fb82ac", driver).Result;
            //var deserializedResponse = JsonConvert.DeserializeObject<CardDetailsResponse>(response);
            //CardTransaction cardTransaction = new CardTransaction
            //{
            //    amount = "10",
            //    currency = "USD",
            //    description = "Funding for jack"
            //};
            var response = cardHelper.FreezeCard("f8e84020-17c5-11e8-94d5-1d3b60fb82ac", driver).Result;
            var deserializedResponse = JsonConvert.DeserializeObject<GenericResponse>(response);
            Console.ReadKey();
        }
    }
}
