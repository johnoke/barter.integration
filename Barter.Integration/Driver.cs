﻿namespace Barter.Integration
{
    public class Driver
    {
        public Driver(string apiKey, string environment)
        {
            this.ApiKey = apiKey;
            this.Environment = environment;
        }
        public string ApiKey { get; set; }
        public string Environment { get; set; }
    }
}
