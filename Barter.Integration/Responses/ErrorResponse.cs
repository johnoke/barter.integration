﻿using System.Collections.Generic;

namespace Barter.Integration.Responses
{
    public class ErrorResponse
    {
        public string status { get; set; }
        public int code { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }
    public class EmailError
    {
        public List<string> email { get; set; }
    }
}
