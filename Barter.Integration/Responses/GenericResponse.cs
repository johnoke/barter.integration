﻿namespace Barter.Integration.Responses
{
    public class GenericResponse
    {
        public string status { get; set; }
        public string message { get; set; }
    }
}
