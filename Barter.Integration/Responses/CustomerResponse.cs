﻿using System.Collections.Generic;

namespace Barter.Integration.Responses
{
    public class CreateCustomerResponse : SuccessResponse
    {
        public CreateCustomerResponseData data { get; set; }
    }
    public class CreateCustomerResponseData
    {
        public CreateCustomerResponseUser user { get; set; }
    }
    public class CreateCustomerResponseUser
    {
        public string id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string bvn { get; set; }
        public string dateofbirth { get; set; }
        public string gender { get; set; }
        public string ssn { get; set; }
        public string image_url { get; set; }
        public string primary_bank_code { get; set; }
        public string primary_account_number { get; set; }
        public string primary_account_name { get; set; }
        public string country { get; set; }
        public int is_subscribed_for_email { get; set; }
        public int is_subscribed_for_push { get; set; }
        public string role { get; set; }
        public string admin_role { get; set; }
        public string corporate_id { get; set; }
        public int email_verified { get; set; }
        public string email_verification_code { get; set; }
        public CreateCustomerResponseWallet wallet { get; set; }
    }
    public class CreateCustomerResponseWallet
    {
        public string id { get; set; }
        public string usd_balance { get; set; }
        public string gbp_balance { get; set; }
        public string eur_balance { get; set; }
        public string kes_balance { get; set; }
        public string ghs_balance { get; set; }
        public string ngn_balance { get; set; }
        public string user_id { get; set; }
        public string corporate_id { get; set; }

    }
    public class GetCustomerByParamResponse
    {
        public string status { get; set; }
        public GetCustomerByParamsData data { get; set; }
    }
    public class GetCustomerByParamsData
    {
        public string id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string bvn { get; set; }
        public string dateofbirth { get; set; }
        public string gender { get; set; }
        public string image_url { get; set; }
        public string primary_bank_code { get; set; }
        public string primary_account_number { get; set; }
        public string primary_account_name { get; set; }
        public string country { get; set; }
        public string corporate_id { get; set; }
        public string full_name { get; set; }
        public int no_cards { get; set; }
        public int no_transactions { get; set; }
    }
    public class GetCustomerByIdResponse
    {
        public string status { get; set; }
        public GetCustomerByIdData data { get; set; }
    }
    public class GetCustomerByIdData
    {
        public GetCustomerByParamsData customer { get; set; }
        public CreateCustomerResponseWallet wallet { get; set; }
        public List<CustomerCardResponse> cards { get; set; }
        public List<CustomerTransactionResponse> transactions { get; set; }
        public int no_cards { get; set; }
        public int no_transactions { get; set; }
    }
    public class CustomerCardResponse
    {
        public string id { get; set; }
        public string user_id { get; set; }
        public string currency { get; set; }
        public string balance { get; set; }
        public string number { get; set; }
        public string cvv { get; set; }
        public string address { get; set; }
        public string expiration { get; set; }
        public string status { get; set; }
        public string virtual_card_id { get; set; }
        public string name { get; set; }
        public string card_type { get; set; }
        public string brand { get; set; }
        public string card_brand { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string address_1 { get; set; }
        public string zip_code { get; set; }
        public string bin { get; set; }
        public string last4 { get; set; }
        public string expiration_month { get; set; }
        public string expiration_year { get; set; }
        public string last_transaction_limit { get; set; }
        public string corporate_id { get; set; }
        public string controlled_by_admin { get; set; }
        public string card_code { get; set; }
        public string third_party_corporate_id { get; set; }
        public bool is_used { get; set; }
    }
    public class CustomerTransactionResponse
    {
        public string id { get; set; }
        public string amount { get; set; }
        public string provider_ref { get; set; }
        public string status { get; set; }
        public string user_id { get; set; }
        public string currency { get; set; }
        public string log { get; set; }
        public string reason { get; set; }
        public string account_number { get; set; }
        public string bank_code { get; set; }
        public string direction { get; set; }
        public string parent_transaction_id { get; set; }
        public bool requires_approval { get; set; }
        public bool is_approved { get; set; }
        public string approved_by { get; set; }
        public string reference { get; set; }
        public string approval_comment { get; set; }
        public string corporate_id { get; set; }
        public string card_id { get; set; }
        public string third_party_corporate_id { get; set; }
    }
    public class GetCustomerCardsResponse
    {
        public string status { get; set; }
        public int total { get; set; }
        public int to { get; set; }
        public int from { get; set; }
        public List<CustomerCardResponse> data { get; set; }
    }
}
