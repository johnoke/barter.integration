﻿using System.Collections.Generic;
namespace Barter.Integration.Responses
{
    public class CardCreationFeeResponse : SuccessResponse
    {
        public List<CardCreationFeeResponseData> data { get; set; }
    }
    public class CardCreationFeeResponseData
    {
        public string id { get; set; }
        public string currency { get; set; }
        public string fee { get; set; }
        public string corporate_id { get; set; }
        public string percentage { get; set; }
        public string physicalcard_fee { get; set; }
    }
    public class FxRatesResponse : SuccessResponse
    {
        public List<FxRatesResponseData> data { get; set; }
    } 
    public class FxRatesResponseData
    {
        public string id { get; set; }
        public string corporate_id { get; set; }
        public string destination_currency { get; set; }
        public string origin_currency { get; set; }
        public string cost_rate { get; set; }
        public string spend_rate { get; set; }
        public string amount_bought { get; set; }
        public long source { get; set; }
        public decimal liquidation_rate { get; set; }
        public long active_source { get; set; }
    }
}
