﻿using System.Collections.Generic;

namespace Barter.Integration.Responses
{
    public class CardResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public CustomerCardResponse data { get; set; }
    }
    public class CardDetailsResponse
    {
        public string status { get; set; }
        public CardDetailsResponseData data { get; set; }
    }
    public class CardDetailsResponseData : CustomerCardResponse
    {
        public List<CustomerTransactionResponse> transactions { get; set; }
        public CreateCustomerResponseUser customer { get; set; }
    }
    public class CardTransactionsResponse
    {
        public string status { get; set; }
        public List<CustomerTransactionResponse> data { get; set; }
    }
}
