﻿using Barter.Integration.Models;
using Barter.Integration.Utilities;
using System.Threading.Tasks;

namespace Barter.Integration.Helpers
{
    public class CardHelper
    {
        public async Task<string> CreateCard(CreateCard createCard, Driver driver)
        {
            var response = await new ApiRequest($"{Uris.CreateCard}").MakeRequest(createCard, headers: GeneralUtil.GetAuthorizationHeader(driver.ApiKey), method: Constants.POST);
            return response;
        }
        public async Task<string> GetCardDetails(string cardId, Driver driver)
        {
            var url = Uris.GetCardDetails.Replace("cardId", cardId);
            var response = await new ApiRequest(url).MakeRequest(headers: GeneralUtil.GetAuthorizationHeader(driver.ApiKey), method: Constants.GET);
            return response;
        }
        public async Task<string> GetCardTransactions(string cardId, Driver driver)
        {
            var url = Uris.GetCardTransactions.Replace("cardId", cardId);
            var response = await new ApiRequest(url).MakeRequest(headers: GeneralUtil.GetAuthorizationHeader(driver.ApiKey), method: Constants.GET);
            return response;
        }
        public async Task<string> FundCard(CardTransaction fundCard, string cardId, Driver driver)
        {
            var url = Uris.FundCard.Replace("cardId", cardId);
            var response = await new ApiRequest(url).MakeRequest(fundCard, headers: GeneralUtil.GetAuthorizationHeader(driver.ApiKey), method: Constants.POST);
            return response;
        }
        public async Task<string> DebitCard(CardTransaction fundCard, string cardId, Driver driver)
        {
            var url = Uris.DebitCard.Replace("cardId", cardId);
            var response = await new ApiRequest(url).MakeRequest(fundCard, headers: GeneralUtil.GetAuthorizationHeader(driver.ApiKey), method: Constants.POST);
            return response;
        }
        public async Task<string> FreezeCard(string cardId, Driver driver)
        {
            var url = Uris.FreezeCard.Replace("cardId", cardId);
            var response = await new ApiRequest(url).MakeRequest(headers: GeneralUtil.GetAuthorizationHeader(driver.ApiKey), method: Constants.POST);
            return response;
        }
        public async Task<string> UnfreezeCard(string cardId, Driver driver)
        {
            var url = Uris.UnfreezeCard.Replace("cardId", cardId);
            var response = await new ApiRequest(url).MakeRequest(headers: GeneralUtil.GetAuthorizationHeader(driver.ApiKey), method: Constants.POST);
            return response;
        }
    }
}
