﻿using Barter.Integration.Utilities;
using System.Threading.Tasks;

namespace Barter.Integration.Helpers
{
    public class FeeHelper
    {
        public async Task<string> GetExchangeRates(Driver driver)
        {
            var response = await new ApiRequest($"{Uris.ExchangeRates}").MakeRequest(headers: GeneralUtil.GetAuthorizationHeader(driver.ApiKey), method: Constants.GET);
            return response;
        }
        public async Task<string> GetCardCreationFees(Driver driver)
        {
            var response = await new ApiRequest($"{Uris.CreationFees}").MakeRequest(headers: GeneralUtil.GetAuthorizationHeader(driver.ApiKey), method: Constants.GET);
            return response;
        }
    }
}