﻿using Barter.Integration.Models;
using Barter.Integration.Utilities;
using System.Threading.Tasks;

namespace Barter.Integration.Helpers
{
    public class CustomerHelper
    {
        public async Task<string> CreateCustomer(CreateCustomer createCustomer, Driver driver)
        {
            var response = await new ApiRequest($"{Uris.CreateCustomer}").MakeRequest(createCustomer, headers: GeneralUtil.GetAuthorizationHeader(driver.ApiKey), method: Constants.POST);
            return response;
        }
        public async Task<string> GetCustomerByParameter(string parameter, string value, Driver driver)
        {
            var url = Uris.GetCustomerByParameter.Replace("parameter", parameter).Replace("strVal", value);
            var response = await new ApiRequest(url).MakeRequest(headers: GeneralUtil.GetAuthorizationHeader(driver.ApiKey), method: Constants.GET);
            return response;
        }
        public async Task<string> GetCustomer(string customerId, Driver driver)
        {
            var url = Uris.GetCustomer.Replace("customerId", customerId);
            var response = await new ApiRequest(url).MakeRequest(headers: GeneralUtil.GetAuthorizationHeader(driver.ApiKey), method: Constants.GET);
            return response;
        }
        public async Task<string> GetCustomerCards(string customerId, Driver driver)
        {
            var url = Uris.GetCustomerCards.Replace("customerId", customerId);
            var response = await new ApiRequest(url).MakeRequest(headers: GeneralUtil.GetAuthorizationHeader(driver.ApiKey), method: Constants.GET);
            return response;
        }
    }
}
