﻿namespace Barter.Integration.Models
{
    public class CreateCustomer
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string account_number { get; set; }
        public string bank_code { get; set; }
        public string bvn { get; set; }
        public string dateofbirth { get; set; }
        public string gender { get; set; }
    }
}
