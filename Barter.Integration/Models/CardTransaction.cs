﻿namespace Barter.Integration.Models
{
    public class CardTransaction
    {
        public string currency { get; set; }
        public string amount { get; set; }
        public string description { get; set; }
    }
}
