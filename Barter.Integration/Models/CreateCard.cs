﻿namespace Barter.Integration.Models
{
    public class CreateCard
    {
        public string currency { get; set; }
        public string amount { get; set; }
        public string customer_id { get; set; }
        public string name { get; set; }
        public string card_band { get; set; }
        public string card_type { get; set; }
    }
}
