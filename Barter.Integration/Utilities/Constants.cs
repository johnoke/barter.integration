﻿namespace Barter.Integration.Utilities
{
    public class Constants
    {
        public const string POST = "POST";
        public const string GET = "GET";
        public const string CONTENTTYPEJSON = "application/json";
        public const string CONTENTTYPEFORMENCODED = "application/x-www-form-urlencoded";
        public const string AUTHORIZATION = "Authorization";
        public const string NONCE = "Nonce";
        public const string SCOPE = "Scope";
        public const string GRANTTYPE = "grant_type";
        public const string BASIC = "Basic";
        public const string TOKEN = "Token";
        public const string TIMESTAMP = "Timestamp";
        public const string TRANSACTIONREFERENCE = "transactionReference";
        public const string MERCHANTCODE = "merchantCode";
    }
}
