﻿using System;
using System.Collections.Generic;

namespace Barter.Integration.Utilities
{
    public class GeneralUtil
    { 
        public static List<KeyValuePair<string, string>> GetAuthorizationHeader(string apiKey)
        {
            var headers = new List<KeyValuePair<string, string>>();
            headers.Add(new KeyValuePair<string, string>(Constants.AUTHORIZATION, $"{Constants.TOKEN} {apiKey}"));
            return headers;
        }
    }
}
