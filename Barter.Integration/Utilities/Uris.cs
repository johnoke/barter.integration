﻿namespace Barter.Integration.Utilities
{
    public class Uris
    {
        public const string CreationFees = "https://api-staging.getbarter.co/corporate/fees/cards";
        public const string ExchangeRates = "https://api-staging.getbarter.co/corporate/fxrates";
        public const string CreateCustomer = "https://api-staging.getbarter.co/corporate/customers/create";
        public const string GetCustomerByParameter = "https://api-staging.getbarter.co/corporate/customers/single?param=parameter&value=strVal";
        public const string GetCustomer = "https://api-staging.getbarter.co/corporate/customers/customerId";
        public const string GetCustomerCards = "https://api-staging.getbarter.co/corporate/customers/customerId/cards";
        public const string CreateCard = "https://api-staging.getbarter.co/corporate/cards/create";
        public const string FundCard = "https://api-staging.getbarter.co/corporate/cards/cardId/fund";
        public const string GetCardDetails = "https://api-staging.getbarter.co/corporate/cards/cardId";
        public const string GetCardTransactions = "https://api-staging.getbarter.co/corporate/cards/cardId/transactions";
        public const string DebitCard = "https://api-staging.getbarter.co/corporate/cards/cardId/debit";
        public const string FreezeCard = "https://api-staging.getbarter.co/corporate/cards/cardId/freeze";
        public const string UnfreezeCard = "https://api-staging.getbarter.co/corporate/cards/cardId/unfreeze";
    }
}
